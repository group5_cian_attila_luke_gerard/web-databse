
<!DOCTYPE HTML>
<html>

    <head>
        <title>Design Assignment</title>
        <meta name="description" content="website description" />
        <meta name="keywords" content="website keywords, website keywords" />
        <meta http-equiv="content-type" content="text/html; charset=windows-1252" />
        <link rel="stylesheet" type="text/css" href="style/style.css" title="style" />
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/jquery.validate.min.js"></script>
        <script src="assets/js/form-validation.js"></script>

    </head>

    <body>
        <div id="main">
            <div id="header">
                <div id="logo">
                    <div id="logo_text">
                        <!-- class="logo_colour", allows you to change the colour of the text -->
                        <h1><a href="index3.html">Doc<span class="logo_colour">View n' Check</span></a></h1>
                        <h2>Doc View n' Check</h2>
                    </div>
                </div>
                <div id="menubar">
                    <ul id="menu">
                        <!-- put class="selected" in the li tag for the selected page - to highlight which page you're on -->
                        <li><a href="index.php">Home</a></li>
                        <!-- <li><a href="examples.html">Examples</a></li>
                        <li><a href="page.html">A Page</a></li>
                        <li><a href="another_page.html">Another Page</a></li> -->
                    </ul>
                </div>
            </div>
            <div id="site_content">
                <div class="sidebar">
                    <!-- insert your sidebar items here -->
                    <h3>Additional Information</h3>
                    <h4>Phone</h4>
                    <p>061 1234567</p>
                    <h4>Email:</h4>
                    <p>docCheck@gmail.com</p>
                    <h4>Website Launched</h4>
                    <h5>March 1st, 2017</h5>
                    <p>Management cannot be responsible for alterations done to project work, use this service at your own discretion</p>
                    <!-- <h3>Useful Links</h3>
                    <ul>
                      <li><a href="#">link 1</a></li>
                      <li><a href="#">link 2</a></li>
                      <li><a href="#">link 3</a></li>
                      <li><a href="#">link 4</a></li>
                    </ul> -->
                    <!-- <h3>Search</h3>
                    <form method="post" action="#" id="search_form">
                      <p>
                        <input class="search" type="text" name="search_field" value="Enter keywords....." />
                        <input name="search" type="image" style="border: 0; margin: 0 0 -9px 5px;" src="style/search.png" alt="Search" title="Search" />
                      </p>
                    </form> -->
                </div>
                <div id="content">
                    <!-- insert the page content here -->


                    <div class="formcontainer">
                        <h2>Registration</h2>
                        <form method="post" name="registration">

                            <label for="firstname">First Name</label>
                            <input type="text" name="first_name" id="first_name" placeholder="John"/>

                            <label for="lastname">Last Name</label>
                            <input type="text" name="last_name" id="last_name" placeholder="Doe"/>

                            <label for="email">Email</label>
                            <input type="email" name="email" id="email" placeholder="john@doe.com"/>

                            <label for="university">University</label>
                            <input type="text" name="university" id="university" placeholder="University"/>

                            <label for="major">Major</label>
                            <input type="text" name="major" id="major" placeholder="Major"/>

                            <label for="password">Password</label>
                            <input type="password" name="password" id="password" placeholder="Please enter password"/>

                            <label for="passcon">Confirm Password</label>
                            <input type="password" name="passcon" id="passcon" placeholder="Please re-enter password"/>

                            <button type="submit" name="submit1" id="submit" value="submit">Register</button>

                        </form>



                        <?php
                        if (isset($_POST["submit1"])) {
                            $link = mysqli_connect("localhost", "root", "");
                            mysqli_select_db($link, "group5");
                            $count = 0;
                            $password = $_POST['password'];
                            $passcon = $_POST['passcon'];
                            $email = $_POST['email'];
                            $allowed = array('ul.ie', 'dcu.ie', 'dcc.ie', 'iua.ie', 'nuigalway.ie', 'maynoothuniversity.ie', 'tcd.ie', 'ucd.ie');
                            $res = mysqli_query($link, "select * from users where email='$_POST[email]'");
                            $count = mysqli_num_rows($res);

                            if ($count > 0) {
                                ?>
                                <script type="text/javascript">
                                    alert("This email already exists");
                                </script>
							<?php
							} elseif ($password != $passcon) {
							?>
                                <script type="text/javascript">
                                    alert("passwords do not match");
                                </script>
                                <?php
                            }

                            $explodedEmail = explode('@', $email);
                            $domain = array_pop($explodedEmail);

                            if (!in_array($domain, $allowed)) {
                                ?>
                                <script type="text/javascript">
                                    alert("Not a valid email, enter one from an Irish University");
                                </script>
                                <?php
                            } else {
                                $pwd = md5($_POST["password"]);
                                $res = mysqli_query($link, "insert into users values('','$_POST[email]','$_POST[first_name]','$_POST[last_name]','','','','$pwd','$_POST[major]','$_POST[university]')");
                                ?>
                                <script type="text/javascript">
                                    window.location = "index.php";
                                </script>
                                <?php
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
    </body>
</html>