<!DOCTYPE HTML>
<html>

    <head>
        <title>Design Assignment</title>
        <meta name="description" content="website description" />
        <meta name="keywords" content="website keywords, website keywords" />
        <meta http-equiv="content-type" content="text/html; charset=windows-1252" />
        <link rel="stylesheet" type="text/css" href="style/style.css" title="style" />
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/jquery.validate.min.js"></script>
        <script src="assets/js/form-validation.js"></script>

    </head>

    <body>
        <div id="main">
            <div id="header">
                <div id="logo">
                    <div id="logo_text">
                        <!-- class="logo_colour", allows you to change the colour of the text -->
                        <h1><a href="index.html">Doc<span class="logo_colour">View n' Check</span></a></h1>
                        <h2>Doc View n' Check</h2>
                    </div>
                </div>
                <div id="menubar">
                    <ul id="menu">
                        <!-- put class="selected" in the li tag for the selected page - to highlight which page you're on -->
                        <li><a href="index2.php">Home</a></li>
                        <!-- <li><a href="examples.html">Examples</a></li>
                        <li><a href="page.html">A Page</a></li>
                        <li><a href="another_page.html">Another Page</a></li> -->
                       
                    </ul>
                </div>
            </div>
            <div id="site_content">
                <div class="sidebar">
                    <!-- insert your sidebar items here -->
                    <h3>Additional Information</h3>
                    <h4>Phone</h4>
                    <p>061 1234567</p>
                    <h4>Email:</h4>
                    <p>docCheck@gmail.com</p>
                    <h4>Website Launched</h4>
                    <h5>March 1st, 2017</h5>
                    <p>Management cannot be responsible for alterations done to project work, use this service at your own discretion</p>
                    <!-- <h3>Useful Links</h3>
                    <ul>
                      <li><a href="#">link 1</a></li>
                      <li><a href="#">link 2</a></li>
                      <li><a href="#">link 3</a></li>
                      <li><a href="#">link 4</a></li>
                    </ul> -->
                    <!-- <h3>Search</h3>
                    <form method="post" action="#" id="search_form">
                      <p>
                        <input class="search" type="text" name="search_field" value="Enter keywords....." />
                        <input name="search" type="image" style="border: 0; margin: 0 0 -9px 5px;" src="style/search.png" alt="Search" title="Search" />
                      </p>
                    </form> -->
                </div>
                <div id="content">
                    <!-- insert the page content here -->


                    <div class="formcontainer">
                        <h2>Submit Task</h2>
                        <form method="post" name="tasks">

                            <label for="taskname">Task Name</label>
                            <input type="text" name="task_name" id="task_name" placeholder="Task Name"/>

                            <label for="nopages"># of Pages</label>
                            <input type="text" name="no_pages" id="no_pages" placeholder="Number of Pages"/>

                            <label for="nowords">Number of Words</label>
                            <input type="text" name="no_words" id="no_words" placeholder="Number of Words"/>

                            <label for="tasktype">Task Type</label>
                            <input type="text" name="task_type" id="task_type" placeholder="Task Type"/>

                            <label for="description">Brief Description</label>
                            <textarea name="description" cols="40" rows="5" id="description" placeholder="Brief Description"></textarea>

                            <label for="description">File Upload</label>
                            <input type="file" name="file" />

                            <button type="submit" name="submit1" id="submit" value="submit">Submit</button>

                        </form>

                        <?php
                        if (isset($_POST["submit1"])) {
                            $link = mysqli_connect("localhost", "root", "");
                            mysqli_select_db($link, "group5");

                            $res = mysqli_query($link, "insert into tasks values('','$_POST[task_name]','','$_POST[no_pages]','$_POST[no_words]','$_POST[task_type]','','','','','$_POST[description]')");

                            if (isset($_FILES['file'])) {
                                $errors = array();
                                $file_name = $_FILES['file']['name'];
                                $file_size = $_FILES['file']['size'];
                                $file_tmp = $_FILES['file']['tmp_name'];
                                $file_type = $_FILES['file']['type'];
                                $file_ext = strtolower(end(explode('.', $_FILES['file']['name'])));

                                $expensions = array("doc", "pdf");

                                if (in_array($file_ext, $expensions) === false) {
                                    ?>
                                    <script type="text/javascript">
                                        alert("wrong type");
                                    </script>
                                    <?php
                                } elseif ($file_size > 2097152) {
                                    ?>
                                    <script type="text/javascript">
                                        alert("file too large");
                                    </script>
            <?php
        } else {
            move_uploaded_file($file_tmp, "files/" . $file_name);
        }
        ?>
                                <script type="text/javascript">
                                    alert("Submitted");
                                </script>
                                <?php
                            }
                        }
                        ?>

                    </div>
                </div>
            </div>
    </body>
</html>
